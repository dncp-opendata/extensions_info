# -*- coding: utf-8 -*-

from mdutils.mdutils import MdUtils

import requests


def write_extension(extension):
    mdFile.new_header(level=2, title=mdFile.new_inline_link(extension['url'],
                                                            extension['titulo']))
    mdFile.new_line(extension['descripcion'])
    mdFile.new_line('')


# tomamos un record cualquiera para traer la lista de extensiones usadas
extensiones = requests.get('https://contrataciones.gov.py/datos/api/v3/doc/ocds/record/'
                           'ocds-03ad3f-331547-2').json()['extensions']

mdFile = MdUtils(file_name='README', title='Extensiones al OCDS usadas por la DNCP')
mdFile.new_paragraph("La Dirección Nacional de Contrataciones Públicas de Paraguay implementa el "
                     + mdFile.new_inline_link('https://standard.open-contracting.org/latest/es/',
                                              'Estándar de Datos Abiertos de Contrataciones Públicas') +
                     ". Pero existen casos en los que los publicadores del estándar cuentan con más "
                     "información que la que el estándar determina, y para publicar esta información "
                     "relevante para el publicador en particular se recurren a las extensiones. "
                     "Estas pueden ser extensiones propias de cada publicador o comunitarias y core, "
                     "que son extensiones que más de un publicador utiliza o que el estándar "
                     "recomienda fuertemente utilizar. En este repositorio se documentan "
                     "todas las extensiones utilizadas por Paraguay.")

extensiones_core = []
extensiones_locales = []
for extension in extensiones:
    extension = requests.get(extension).json()
    documentation_url = extension['documentationUrl']['es'] if 'es' in extension['documentationUrl'] else \
        extension['documentationUrl']['en']
    extension_dict = {'url': extension['documentationUrl']['es'] if 'es' in extension['documentationUrl'] else
                      extension['documentationUrl']['en'],
                      'descripcion': extension['description']['es'] if 'es' in extension['description'] else
                      extension['description']['en'],
                      'titulo': extension['name']['es'] if 'es' in extension['name'] else
                      extension['name']['en'],
                      'local': 'dncp' in documentation_url
                      }
    if extension_dict['local']:
        extensiones_locales.append(extension_dict)
    else:
        extensiones_core.append(extension_dict)

mdFile.new_header(title="Extensiones Locales", level=1)
for extension in extensiones_locales:
    write_extension(extension)


mdFile.new_header(title="Extensiones Core o Comunitarias", level=1)
for extension in extensiones_core:
    write_extension(extension)

mdFile.create_md_file()

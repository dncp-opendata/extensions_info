
Extensiones al OCDS usadas por la DNCP
======================================


La Dirección Nacional de Contrataciones Públicas de Paraguay implementa el [Estándar de Datos Abiertos de Contrataciones Públicas](https://standard.open-contracting.org/latest/es/). Pero existen casos en los que los publicadores del estándar cuentan con más información que la que el estándar determina, y para publicar esta información relevante para el publicador en particular se recurren a las extensiones. Estas pueden ser extensiones propias de cada publicador o comunitarias y core, que son extensiones que más de un publicador utiliza o que el estándar recomienda fuertemente utilizar. En este repositorio se documentan todas las extensiones utilizadas por Paraguay.
# Extensiones Locales

## [Proyectos de Inversión](https://gitlab.com/dncp-opendata/ocds_planning_investmentproject_extension)
  
Agrega una lista de proyectos de infraestructura, incluyendo su id, nombre y URI a la planificación y contratos del proceso de contratación, para realizar el seguimiento entre los proyectos de infraestructura y su ejecución  

## [Juntas de aclaración](https://gitlab.com/dncp-opendata/ocds_clarification_meetings_extension)
  
Esta extensión agrega información sobre la fecha  y lugar en la que una junta de aclaración sobre un proceso de contratación se llevará acabao.  

## [Atributos de los items](https://gitlab.com/dncp-opendata/ocds_item_attributes_extension)
  
Agrega una lista genérica de atributos al objeto Item. Cada objeto atributo contiene un nombre, valor e identificador. Un atributo puede ser por ejemplo la MARCA del item.  

## [Items de la Planificación](https://gitlab.com/dncp-opendata/ocds_planning_items_extension)
  
Agrega una lista de items a nivel de planificación. Estos ítems estan a un nivel más general que los ítems utilizados a nivel de la convocatoria y sirven de referencia general a qué es lo que se va a comprar  

## [Actividades del Proveedor](https://gitlab.com/dncp-opendata/ocds_partyDetails_activityType_extension/)
  
Para clasificar si el proveedor provee bienes, servicios o ambos  

## [Proveedores Invitados a la Convocatoria](https://gitlab.com/dncp-opendata/ocds_tender_notifiedsuppliers_extension/)
  
Agrega la lista de los proveedores que han sido invitados a ofertar en esta convocatoria.  

## [Detalles sobre Lotes](https://gitlab.com/dncp-opendata/ocds_tender_lots_details_extension)
  
Agrega información específica sobre los lotes en Paraguay. Esta información incluye si el lote es de abastecimiento simultáneo, el estado del lote, el monto mínimo (para contratos abiertos por monto) y el tipo de contrato abierto, que puede ser por monto o por cantidad o ninguno  

## [Detalles sobre la Clasficación de las Entidades Públicas](https://gitlab.com/dncp-opendata/ocds_partyDetails_publicEntitiesDetails_extension)
  
Detalles relacionados a las entidades públicas en el Paraguay. Agrega los campos level (ejemplo Gobiernos Departamentales), legalEntityTypeDetail, entityType (ej Municipalidades) y type (entidad o unidad de contratación)  

## [SubItems de los Items](https://gitlab.com/dncp-opendata/ocds_items_subItems_extension)
  
Agrega una lista de sub items al objeto items  

## [Detalles del Estado](https://gitlab.com/dncp-opendata/ocds_statusdetails_extension)
  
Agrega el campo statusDetails a Tender, Award y Contract para detallar la descripción local del estado de la convocatoria, adjudicacioń y contrato  

## [Detalles sobre la Categoría Principal del Proceso de Contratación](https://gitlab.com/dncp-opendata/ocds_mainProcurementCategoryDetails_extension)
  
Agrega el campo mainProcurementCategoryDetails que puede ser utilizado para proveer el nombre local de la categoría del proceso de contratación.  

## [Identificador de la planificación](https://gitlab.com/dncp-opendata/ocds_planning_identifier_extension)
  
Agrega un identificador para la planificación del proceso de contratación. En algunos casos este identificador puede coincidir con el identificador del proceso de contratación (ocid)  

## [Fecha Estimada de la Convocatoria durante la Planificación](https://gitlab.com/dncp-opendata/ocds_planning_estimatedDate_extension)
  
Una fecha estimada de la convocatoria en la etapa de planificación.  

## [Detalles sobre Adendas](https://gitlab.com/dncp-opendata/ocds_amendments_details_extension)
  
Agrega información extra al objeto Amendment. Se utiliza para las adendas a los contratos, especificando el amendsPeriod, para indicar la nueva duración del contrato o amendsAmount para indicar el monto adicionado al contrato.  

## [Detalle sobre Lotes](https://gitlab.com/dncp-opendata/ocds_lot_details_extension)
  
Agrega información específica sobre los lotes en Paraguay. Esta información incluye si el lote es de abastecimiento simultáneo, el estado del lote, el monto mínimo (para contratos abiertos por monto) y el tipo de contrato abierto, que puede ser por monto o por cantidad o ninguno  

## [Detalles de Transacciones (Pagos)](https://gitlab.com/dncp-opendata/ocds_transaction_details_extension)
  
Agrega el campo sourceSystem para identificar si el pago proviene de los registros del Ministerio de Hacienda (SIAF) o de los registros de la DNCP (SICP). Además agrega el campo finacialCode que contine el código de contratación del contrato o adenda para identificar a qué contrato o adenda se está realizando el pago.  

## [Código de Contratación del detalle de la Implementación del Contrato](https://gitlab.com/dncp-opendata/ocds_contract_financialprogress_financialcode_extension)
  
Agrega el código de contración, en el campo financialCode, a la línea presupuestaria utilizada para realizar pagos al contrato. Este campo sirve para identificar qué linea está siendo utilizada para pagar qué contrato o adenda identificado por su código de contratación.  

## [Detalles sobre las Categorías de una Parte](https://gitlab.com/dncp-opendata/ocds_partyDetails_categories_extension)
  
Se agrega las categorías en la que está registrado un proveedor. Estas categorías están definidas por el publicador, pueden ser por ejemplo: Pasajes Y transportes, Capacitaciones y Adiestramientos, etc   

## [Protestas](https://gitlab.com/dncp-opendata/ocds_complaints_extension)
  
Agrega una lista de protestas realizadas contra el proceso de contratación. Se incluye la información de los intervinientes en la protesta, que incluye los protestantes, los jueces y actuarios y el protestado. También la fecha de la protesta, su estado, el tipo de protesta (contra el pliego de bases y condiciones o contra el resultado), y los eventos que sucedieron durante el trámite de la protesta.  

## [Cantidad Mínima del Item](https://gitlab.com/dncp-opendata/ocds_item_minQuantity_extension)
  
Agrega el campo de cantidad mínima al ítem. Aplica a los casos en los que el contrato es del tipo abierto por cantidad.  

## [Órdenes de Compra de la Implementación del Contrato](https://gitlab.com/dncp-opendata/ocds_contract_implementation_purchaseOrder_extension)
  
Agrega la información de las órdenes de compra realizadas durante la implementación de un contrato. Se incluye la información del periodo de ejecución de la orden, el estado de la misma, el título y un identificador. Además se agrega la relación entre un milestone y su orden de compra relacionada, si la tuviera.  

## [Detalles del tipo de documento](https://gitlab.com/dncp-opendata/ocds_document_type_details)
  
Nombre local utilizado para el tipo de documento. Por ejemplo para el código del estándar para documentType 'awardNotice', un nombre local podría ser 'Resolución de la Adjudicación'  

## [Detalles sobre Datos relacionados a una Adenda Cancelada](https://gitlab.com/dncp-opendata/ocds_contract_cancellations_amendments_extension)
  
Información detallada sobre una adenda a un contrato que fue cancelada. Se incluye la información de si se realizó un pago anticipado a la adenda del contrato, si se aplicó la garantía y el porcentaje de la misma.  

## [Items ofertados](https://gitlab.com/dncp-opendata/ocds_bid_items_extension)
  
Se agrega el atributo items a las ofertas para poder desglosar por item el precio y unidad por la cual se está realizando la oferta.  

## [Subastas](https://gitlab.com/dncp-opendata/ocds_auctions_extension)
  
Agrega la infromación sobre las subastas realizadas en el proceso de contratación. Esta información incluye todas las ofertas realizadas durante la competencia, con sus items y fechas y horas exactas, así como el ganador de la subasta.  

## [Segunda Etapa](https://gitlab.com/dncp-opendata/ocds_secondStage_extension)
  
Agrega un objeto secondStage al proceso de contratación para incluir la información de los procesos que tienen más de una etapa. Estos procesos pueden ser por ejemplo las precalificaciones, en la que la primera etapa es la precalificación y la segunda la convocatoria posterior. También se utiliza en convenios marco, donde la primera etapa del proceso es la formación del convenio y las siguientes las competencias o compras que se realizan en el marco del convenio.  

# Extensiones Core o Comunitarias

## [Budget Breakdown](https://extensions.open-contracting.org/en/extensions/budget/)
  
Adds a budget breakdown array to the budget object to break down a budget by source and period.  

## [Budget and spending classification](https://extensions.open-contracting.org/en/extensions/budget_and_spend/)
  
Extends budget breakdown and contract implementation to allow publication of detailed budget allocations and execution for a contracting process, using classifications that can be mapped to separately published budget and spend data.  

## [Lots](https://extensions.open-contracting.org/en/extensions/lots/)
  
A tender process may be divided into lots, where bidders can bid on one or more lots. Details of each lot can be provided here. Items, documents and other features can then reference the lot they are related to using relatedLot. Where no relatedLot identifier is given, the values should be interpreted as applicable to the whole tender.  

## [Enquiries](https://extensions.open-contracting.org/en/extensions/enquiries/)
  
The enquiries extension can be used to record questions raised during a contracting process, and the answers provided.  

## [enquiriesAddress](https://github.com/sdd1982/enquiriesAddress/blob/master/README.md)
  
Permite la escritura del lugar de aclaraciones al proceso de contratación  

## [legalEntityTypeDetail](https://github.com/sdd1982/legalEntityTypeDetail/blob/master/README.md)
  
Indica el tipo de organización en Colombia: Limitada, Sociedad anónima, Sociedad anónima simplificada, entre otros  

## [Transaction Additional Fields for Paraguay](https://github.com/gitMHOCDS/transactions)
  
Support for finantialObligation and paymentRequest, according to the payment process for Paraguay  

## [Contract suppliers](https://extensions.open-contracting.org/en/extensions/contract_suppliers/)
  
To allow explicit declaration of suppliers within the contracts block. Used when a single award to multiple suppliers results in multiple contracts to a sub-set of those awarded suppliers.  

## [Party Scale](https://extensions.open-contracting.org/en/extensions/partyScale/)
  
For classifying organizations as micro, sme or large.  

## [Location](https://extensions.open-contracting.org/en/extensions/location/)
  
Allows the point of delivery or site of works for a given line item to be indicated in tender, award and contract objects.  

## [Member Of](https://github.com/open-contracting-extensions/ocds_memberOf_extension)
  
Adds a field to indicate whether a party is a member or sub-organization of another party, association or grouping.  

## [Tender Date Published](https://github.com/portaledcahn/ocds_tenderDatePublished_extension/blob/master/README.md)
  
Adds a date field to indicate when the tender was published.  

## [Guarantees](https://github.com/INAImexico/ocds_guarantees_extension)
  
Some procurement processes consider the specification of guarantees in order to ensure compliance with the terms of a contract.

There are many types of guarantees, so we consider the creation of a new extension based on the formats that are required for the Federal Treasury (Mexico). See discussion in https://github.com/open-contracting/standard/issues/651  

## [Sources](https://bitbucket.org/ONCAETI/ocds_releasesource_extension/src/master/README.md)
  
Adds a sources array to indicate the information systems from which the data originates.  

## [Requirements](https://extensions.open-contracting.org/en/extensions/requirements/)
  
Adds a criteria section to the tender object and responses sections to bids, awards and contracts. Based on the EU's Core Criterion and Core Evidence Vocabulary (CCCEV).  

## [Bid opening](https://extensions.open-contracting.org/en/extensions/bidOpening/)
  
Adds an object to describe the date, time, place and other details of the bid opening.  

## [Covered By](https://extensions.open-contracting.org/en/extensions/coveredBy/)
  
Adds a field to indicate the treaties, laws, regulations or other instruments that the contracting process is covered by.  

## [Requirements](https://extensions.open-contracting.org/en/extensions/requirements/)
  
Adds a criteria section to the tender object and responses sections to bids, awards and contracts. Based on the EU's Core Criterion and Core Evidence Vocabulary (CCCEV).  

## [Participation Fees](https://extensions.open-contracting.org/en/extensions/participation_fee/)
  
Where a tender process involves payment of fees to access documents, submit a proposal, or be awarded a contract, this extension can be used to provide fee details.  

## [Milestone documents](https://extensions.open-contracting.org/en/extensions/milestone_documents/)
  
Documents at the milestone level were deprecated in OCDS 1.1. This extension re-introduces the ability to attach documents to each individual milestone.  

## [Techniques](https://extensions.open-contracting.org/en/extensions/techniques/)
  
Adds fields to the tender and lot objects to describe the use of techniques, such as framework agreements, dynamic purchasing systems and electronic auctions.  

## [Requirements](https://extensions.open-contracting.org/en/extensions/requirements/)
  
Adds a criteria section to the tender object and responses sections to bids, awards and contracts. Based on the EU's Core Criterion and Core Evidence Vocabulary (CCCEV).  

## [Bid statistics and details](https://extensions.open-contracting.org/en/extensions/bids/)
  
Allowing bid statistics, and detailed bid information to be represented.  
